install:
	@python setup.py install

install-test: install
	@pip install -r requirements-test.txt

lint:
	@flake8 dkrls
	@echo "Lint OK"

test:
	@pytest tests

e2e:
	@./tests/e2e.sh

e2e-debug:
	@./tests/e2e.sh --debug

docker:
	@docker build --tag dkr-ls .

make docker-alias:
	@echo 'alias dkr-ls='\''docker run --rm -v /var/run/docker.sock:/var/run/docker.sock dkr-ls'\'''