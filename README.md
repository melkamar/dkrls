# dkr-ls

CLI tool for listing Docker images in the local repository. The tool supports
several filtering options not available in the `docker images` command.

This is an implementation of the [pipetail interview task](https://github.com/pipetail/interviews/blob/1.0/README.md).

## Quickstart
There are two ways of using the tool. It can be installed onto your local 
system (requires Python3.6+), or it can be run from a Docker image.

### Local installation
To use on your local system, requirements are:
- Python 3.6+
- `pip`
- `setuptools` 

```
$ make install
$ dkr-ls --help
```

### Docker
To run the tool from a docker image, you only need to have Docker installed. 
The command is rather long, so it's useful to create an alias for it. See the 
alias command in the [Makefile](Makefile).

```
$ make docker
$ eval $(make docker-alias)
$ dkr-ls --help
```

## Development

The source code of the project is split into the following structure:
```
<root>
|- dkrls/            - python sources
|    |- cli.py       - command-line interface
|    |- commands.py  - implementation of the commands' logic
|    |- dkr.py       - code for interfacing with Docker
|    |- formatter.py - formatting the CLI stdout 
|    `- model.py     - model classes
|
|- tests/            - tests (pytest + e2e) 
`- setup.py          - setuptools definition of the project
```

### Python tests
There is a limited `pytest` suite available for the tool. To run it, do:

```
$ make install-test
$ make test
```

### End-to-end tests
A script with end-to-end scripts is available in [tests/e2e.sh](tests/e2e.sh).
To run the tests, do:

```
$ make e2e
```

Note: this will pull a few images from the Docker Hub and build a custom image.
Those images will be removed at the end of the test. If you already had some
of the images in your registry, **they will be removed**.