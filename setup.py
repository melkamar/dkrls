from setuptools import setup
from dkrls.__version__ import version

setup(
    name='dkrls',
    version=version,
    description='List various ',
    author='Martin Melka',
    author_email='melka@avast.com',
    license='MIT',
    keywords='extension, browser, chrome, firefox, store',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Topic :: Software Development'
    ],
    url='https://gitlab.com/melkamar/dkrls',
    entry_points={
        'console_scripts': [
            'dkr-ls = dkrls.cli:main'
        ]
    },
    install_requires=['click', 'docker', 'timeago'],
    setup_requires=[],
    tests_require=['pytest', 'flake8']
)
