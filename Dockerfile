FROM python:3.8-alpine
COPY . /dkrls
WORKDIR /dkrls

RUN python setup.py install
ENTRYPOINT ["python", "-m", "dkrls"]
