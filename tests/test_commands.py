from dkrls import model, commands
from time import time

import pytest


@pytest.mark.parametrize("min_age, expected_image_names", [
    [1, [
        '1 day 5 hours ago',
        '7 days 1 hour ago',
        '1 year 1 hour ago',
    ]],
    [2, [
        '7 days 1 hour ago',
        '1 year 1 hour ago',
    ]],
    [7, [
        '7 days 1 hour ago',
        '1 year 1 hour ago',
    ]],
    [8, [
        '1 year 1 hour ago',
    ]],
    [365, [
        '1 year 1 hour ago',
    ]],
    [366, [
    ]],
])
def test_filter_min_age(min_age, expected_image_names):
    def _create_image(name: str, created: float):
        return model.Image(
            name,
            'n/a',
            int(created),
            0
        )

    _images = [
        _create_image('1 min ago', time() - 60),
        _create_image('1 hour ago', time() - 60 * 60),
        _create_image('18 hours ago', time() - 60 * 60 * 18),
        _create_image('1 day 5 hours ago', time() - (24 + 5) * 60 * 60),
        _create_image('7 days 1 hour ago', time() - (24 * 7 + 1) * 60 * 60),
        _create_image('1 year 1 hour ago', time() - (24 * 365 + 1) * 60 * 60),
    ]

    filtered = commands._filter_min_age(_images, min_age)

    assert len(filtered) == len(expected_image_names)
    for img in filtered:
        assert img.name in expected_image_names


@pytest.mark.parametrize("prefixes, expected_image_names", [
    [
        ['foo'],
        [
            'foo:latest',
        ]
    ],
    [
        ['bar'],
        [
            'bar:abcd',
            'bartender:something',
        ]
    ],
    [
        ['b', 'foo'],
        [
            'foo:latest',
            'bar:abcd',
            'bartender:something',
            'bust:amove',
        ]
    ],
    [
        ['lorem', 'b'],
        [
            'bar:abcd',
            'bartender:something',
            'bust:amove',
            'lorem:ipsum',
            'lorem:dolor',
        ]
    ],
    [
        [],
        [
        ]
    ],
    [
        ['foo', 'b', 'lorem'],
        [
            'foo:latest',
            'bar:abcd',
            'bartender:something',
            'bust:amove',
            'lorem:ipsum',
            'lorem:dolor',
        ]
    ],
])
def test_filter_prefixes(prefixes, expected_image_names):
    def _create_image(name: str):
        return model.Image(name, 'n/a', 0, 0)

    _images = [
        _create_image('foo:latest'),
        _create_image('bar:abcd'),
        _create_image('bartender:something'),
        _create_image('bust:amove'),
        _create_image('lorem:ipsum'),
        _create_image('lorem:dolor'),
    ]

    filtered = commands._filter_prefixes(_images, prefixes)

    assert len(filtered) == len(expected_image_names)
    for img in filtered:
        assert img.name in expected_image_names


def test_max_tags():
    def _create_image(name: str):
        return model.Image(name, 'n/a', 0, 0)

    _images = [
        _create_image('foo:latest'),
        _create_image('foo:a'),
        _create_image('foo:b'),
        _create_image('bar:abcd'),
        _create_image('bartender:something'),
        _create_image('bust:amove'),
        _create_image('lorem:ipsum'),
        _create_image('lorem:dolor'),
    ]

    filtered = commands._filter_max_tags(_images, 1)
    filtered_names = [img.name for img in filtered]

    assert 'foo:latest' in filtered_names
    assert 'foo:a' not in filtered_names
    assert 'foo:b' not in filtered_names
    assert 'bar:abcd' in filtered_names
    assert 'bartender:something' in filtered_names
    assert 'bust:amove' in filtered_names
    assert 'lorem:ipsum' in filtered_names
    assert 'lorem:dolor' not in filtered_names


@pytest.mark.parametrize("min_size, expected_image_names", [
    ['1', [
        '500B',
        '10KB',
        '42MB',
        '50.5MB',
        '666GB',
        '1PB',
    ]],
    ['400B', [
        '500B',
        '10KB',
        '42MB',
        '50.5MB',
        '666GB',
        '1PB',
    ]],
    ['900B', [
        '10KB',
        '42MB',
        '50.5MB',
        '666GB',
        '1PB',
    ]],
    ['40MB', [
        '42MB',
        '50.5MB',
        '666GB',
        '1PB',
    ]],
    ['50MB', [
        '50.5MB',
        '666GB',
        '1PB',
    ]],
    ['50.6MB', [
        '666GB',
        '1PB',
    ]],
    ['700GB', [
        '1PB',
    ]],
])
def test_min_size(min_size, expected_image_names):
    def _create_image(name: str, size: int):
        return model.Image(name, 'n/a', 0, size)

    _images = [
        _create_image('500B', 500),
        _create_image('10KB', 10 * 1024),
        _create_image('42MB', 42 * 1024 ** 2),
        _create_image('50.5MB', 50.5 * 1024 ** 2),
        _create_image('666GB', 666 * 1024 ** 3),
        _create_image('1PB', 1 * 1024 ** 5),
    ]

    filtered = commands._filter_min_size(_images, min_size)

    assert len(filtered) == len(expected_image_names)
    for img in filtered:
        assert img.name in expected_image_names
