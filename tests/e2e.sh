#!/bin/bash

set -eE

LOG_ARGS=''

## Arg parsing
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -d|--debug)
    LOG_ARGS='--log-level DEBUG'
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters
## Eof arg parsing


#
# Populating the local Docker registry
#
echo ---------------------------------------------------------
echo "Populating some Docker images into the registry..."
echo ""
IMAGES_TO_PULL='
  nginx:latest
  nginx:alpine
  mysql/mysql-server:5.7
  datadog/agent:7
  debian:buster-20200514-slim
'
for img in $IMAGES_TO_PULL; do
  docker pull "$img"
done

# Build an image and tag something over it to create a <none>:<none> entry
docker build --tag custombuilt:image tests
docker tag nginx:latest custombuilt:image

echo "Done populating Docker images, running tests now."

set +eE

#
# Running e2e tests
#
echo ---------------------------------------------------------
set -x
dkr-ls $LOG_ARGS repos
set +x

echo ---------------------------------------------------------
set -x
dkr-ls $LOG_ARGS tags
set +x

echo ---------------------------------------------------------
set -x
dkr-ls $LOG_ARGS repos --maxTags 3
set +x

echo ---------------------------------------------------------
set -x
dkr-ls $LOG_ARGS tags --prefix nginx --prefix debian
set +x

echo ---------------------------------------------------------
set -x
dkr-ls $LOG_ARGS tags --maxTags 1 --prefix nginx --prefix debian --sum=true
set +x

echo ---------------------------------------------------------
set -x
dkr-ls $LOG_ARGS tags --minAge 20 --minSize 100MB
set +x

echo ---------------------------------------------------------

#
# Cleaning the local Docker registry from the images that were pulled
#
for img in $IMAGES_TO_PULL; do
  echo "Removing image $img"
  docker rmi "$img"
done
docker rmi custombuilt:image

echo
echo "e2e done."