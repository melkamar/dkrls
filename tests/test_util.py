import pytest

from dkrls import model


@pytest.mark.parametrize('size,expected_human', [
    [500, '500B'],
    [int(3.5 * 1024), '3.5KB'],
    [int(4.2 * 1024 ** 2), '4.2MB'],
    [int(5 * 1024 ** 3), '5GB'],
    [int(5.42675 * 1024 ** 3), '5.4GB'],
])
def test_bytes_to_human(size, expected_human):
    size_human = model.bytes_to_human_bytes(size)
    assert size_human == expected_human


@pytest.mark.parametrize('human_size,expected_size', [
    ['500', 500],
    ['500B', 500],
    ['42KB', 42 * 1024],
    ['18MB', 18 * 1024 ** 2],
    ['50GB', 50 * 1024 ** 3],
])
def test_human_to_bytes(human_size, expected_size):
    size = model.human_bytes_to_bytes(human_size)
    assert size == expected_size
