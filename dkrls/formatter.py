"""
Module for formatting CLI output.
"""

import logging
from typing import List, Optional

from dkrls import model

logger = logging.getLogger(__name__)


def format_repos(repos: List[str]) -> str:
    return "REPOSITORY\n{}".format("\n".join(repos))


def format_tags(images: List[model.Image],
                size_sum: Optional[str] = None) -> str:
    """
    Create a formatted string out of the images to look like the following:

    REPOSITORY  TAG     IMAGE ID      CREATED      SIZE
    nginx       latest  7e4d58f0e5f3  2 weeks ago  133MB
    nginx       alpine  6f715d38cfe0  6 weeks ago  22.1MB
                                     [TOTAL        42GB]

    :param images: list of images to be formatted
    :param size_sum: if provided, a total size will be added to the result
    :return: a formatted string
    """

    def _make_line(columns: List[str]) -> str:
        return (
            f'{columns[0]:{col_widths[0]}}{" " * col_spacing}'
            f'{columns[1]:{col_widths[1]}}{" " * col_spacing}'
            f'{columns[2]:{col_widths[2]}}{" " * col_spacing}'
            f'{columns[3]:{col_widths[3]}}{" " * col_spacing}'
            f'{columns[4]}'
        )

    result = []
    lines = [
        ['REPOSITORY', 'TAG', 'IMAGE ID', 'CREATED', 'SIZE'],
        *[_image_to_cols(image) for image in images]
    ]

    logger.debug(f'Formatting lines: {lines}')

    col_spacing = 6
    col_widths = [0, 0, 0, 0, 0]
    for line in lines:
        for idx, txt in enumerate(line):
            col_widths[idx] = max(col_widths[idx], len(txt))

    for line in lines:
        result.append(_make_line(line))

    if size_sum:
        result.append(_make_line(['', '', '', 'TOTAL', size_sum]))

    return "\n".join(result)


def _image_to_cols(image: model.Image) -> List[str]:
    """
    Convert an Image object to the five columns expected by the 'tags'
    output format.
    """
    return [
        image.repository,
        image.tag,
        image.short_id,
        image.created_human,
        image.size_human
    ]
