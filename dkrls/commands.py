"""
Implementation of the commands' logic.
"""

import logging
import time
from dataclasses import dataclass
from typing import List, Optional

from dkrls import dkr, model

logger = logging.getLogger(__name__)


@dataclass
class TagsResult:
    images: List[model.Image]
    size: Optional[str]


def tags(
        minage: Optional[int],
        prefixes: Optional[List[str]],
        maxtags: Optional[int],
        minsize: Optional[str],
        sum_: bool
) -> TagsResult:
    """
    Implementation of the 'tags' command. The function parameters correspond
    to the CLI options.
    """
    images = dkr.get_images()
    logger.debug(f'{len(images)} images before filtering.')

    if minage is not None:
        images = _filter_min_age(images, minage)
        logger.debug(f'{len(images)} images after filtering for min age.')

    if prefixes is not None and len(prefixes) > 0:
        images = _filter_prefixes(images, prefixes)
        logger.debug(f'{len(images)} images after filtering for prefixes.')

    if maxtags is not None:
        images = _filter_max_tags(images, maxtags)
        logger.debug(f'{len(images)} images after filtering for max tags.')

    if minsize is not None:
        images = _filter_min_size(images, minsize)
        logger.debug(f'{len(images)} images after filtering for min size.')

    total_size = _count_total_size(images) if sum_ else None

    return TagsResult(images, total_size)


def _filter_min_age(images: List[model.Image],
                    min_age: int) -> List[model.Image]:
    return [
        image for image in images
        if (time.time() - image.created) / (60 * 60 * 24) > min_age
    ]


def _filter_prefixes(images: List[model.Image],
                     prefixes: List[str]) -> List[model.Image]:
    return [
        image for image in images
        if any(image.name.startswith(prefix) for prefix in prefixes)
    ]


def _filter_max_tags(images: List[model.Image],
                     max_tags: int) -> List[model.Image]:
    repo_to_images = {}
    for image in images:
        if image.repository not in repo_to_images:
            repo_to_images[image.repository] = []
        if len(repo_to_images[image.repository]) < max_tags:
            repo_to_images[image.repository].append(image)

    return [
        img
        for grouped_images in repo_to_images.values()
        for img in grouped_images
    ]


def _filter_min_size(images: List[model.Image],
                     min_size: str) -> List[model.Image]:
    return [
        image for image in images
        if image.size > model.human_bytes_to_bytes(min_size)
    ]


def _count_total_size(images: List[model.Image]) -> str:
    return model.bytes_to_human_bytes(
        sum([
            image.size
            for image in images
        ])
    )
