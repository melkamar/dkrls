import logging
import re

import timeago

logger = logging.getLogger(__name__)

_SIZE_POWER_TO_SUFFIX = {
    0: 'B',
    1: 'KB',
    2: 'MB',
    3: 'GB',
    4: 'TB'
}

# This could be computed from the above list but hardcoding saves some perf.
# And it's not like this is ever going to change.
_SUFFIX_TO_SIZE_POWER = {
    'B': 0,
    'KB': 1,
    'MB': 2,
    'GB': 3,
    'TB': 4,
}


class CliException(Exception):
    def __init__(self, message: str):
        self.message = message


class Image:
    """
    A Docker image.
    """

    def __init__(self, name: str, id_: str, created: int, size: int):
        """
        :param name: the full name of the image, e.g. mongo:4.2
        :param id_: the ID of the image
        :param created: unix timestamp of when the image was created
        :param size: size of the whole image in bytes
        """
        self.name = name
        self.id = id_
        self.created = created
        self.size = size

    @property
    def short_id(self):
        """
        First 12 characters from the image ID.
        """
        return self.id[:12]

    @property
    def created_human(self):
        """
        The date of creation in a human-readable format, e.g. '2 months ago'
        """
        return timeago.format(self.created)

    @property
    def size_human(self):
        """
        The size of the image in a human-readable format, e.g. '322MB'
        """
        return bytes_to_human_bytes(self.size)

    @property
    def repository(self):
        """
        The repository of the image, determined from the name.

        For the name 'myregistryhost:5000/fedora/httpd:version1.0', this
        returns 'myregistryhost:5000/fedora/httpd'.
        """
        return self.name.rsplit(':', maxsplit=1)[0]

    @property
    def tag(self):
        """
        The tag of the image, determined from the name.

        For the name 'myregistryhost:5000/fedora/httpd:version1.0', this
        returns 'version1.0'.
        """
        return self.name.rsplit(':', maxsplit=1)[1]


def bytes_to_human_bytes(size_bytes: int) -> str:
    power = 2 ** 10
    n = 0
    size = size_bytes
    while size > power:
        size /= power
        n += 1

    rounded_size = round(size, 1)
    # If the size number is really close to its floored value,
    # convert it into an int (5.0 -> 5)
    if rounded_size - int(rounded_size) < 0.0000000001:
        rounded_size = int(rounded_size)

    human_bytes = f'{rounded_size}{_SIZE_POWER_TO_SUFFIX[n]}'
    logger.debug(f'Converted {size_bytes} bytes to "{human_bytes}"')
    return human_bytes


def human_bytes_to_bytes(human_bytes: str) -> int:
    pattern = r'^(?P<size>\d+(\.\d+)?)(?P<suffix>[a-zA-Z]*)?$'
    match = re.match(pattern, human_bytes)
    if not match:
        raise CliException(
            f'Cannot parse "{human_bytes}" as a human-readable size. '
            f'Expected a format like "42MB".')

    size = float(match.group('size'))
    suffix = match.group('suffix').upper()
    if not suffix:
        suffix = 'B'

    try:
        suffix_power = _SUFFIX_TO_SIZE_POWER[suffix]
    except KeyError:
        raise CliException(
            f'Cannot parse "{suffix}" as a size suffix. Expected one of: '
            f'{_SUFFIX_TO_SIZE_POWER.keys()}')

    bytes_ = size * 1024 ** suffix_power
    logger.debug(f'Converted "{human_bytes}" to {bytes_} bytes')
    return bytes_
