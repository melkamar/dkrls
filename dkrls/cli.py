"""
Command line interface of the program
"""

import logging
from typing import List, Optional

import click

from dkrls import formatter, dkr, commands, model, __version__


@click.group('dkr-ls')
@click.option('--log-level',
              type=click.Choice(['DEBUG', 'INFO', 'WARNING', 'ERROR']),
              default='WARNING',
              help='Set the logging level.'
              )
@click.version_option(__version__.version)
def cli(log_level: str):
    logging.basicConfig(
        level=logging.getLevelName(log_level),
        format='%(asctime)s %(name)s %(levelname)-8s %(message)s'
    )


@cli.command('repos',
             help='List Docker repositories. Prints a list of all '
                  'repositories in the local registry with at least one tag '
                  'in them')
def repos():
    try:
        repos_list = dkr.get_repos()
        print(formatter.format_repos(repos_list))
    except model.CliException as e:
        print(e.message)
        exit(1)


@cli.command('tags', help='List Docker image tags and their details.')
@click.option('--minAge',
              type=int,
              help='List only images older than minAge days.'
              )
@click.option('--prefix',
              type=str,
              multiple=True,
              help='List only images with these repository prefixes. Option '
                   'may be repeated multiple times.'
              )
@click.option('--maxTags',
              type=int,
              help='List only maxTags number of tags of each image.'
              )
@click.option('--minSize',
              type=str,
              help='List only images bigger than minSize in human-readable '
                   'format.'
              )
@click.option('--sum',
              type=bool,
              default=False,
              help='Sum all image sizes and print them at the end of the '
                   'listing.'
              )
def tags(minage: Optional[int],
         prefix: Optional[List[str]],
         maxtags: Optional[int],
         minsize: Optional[str],
         sum: bool):
    try:
        cmd_result = commands.tags(minage, prefix, maxtags, minsize, sum)
        print(formatter.format_tags(cmd_result.images, cmd_result.size))
    except model.CliException as e:
        print(e.message)
        exit(1)


def main():
    cli()


if __name__ == '__main__':
    main()
