"""
Module for interfacing with the Docker daemon.
"""

import logging
from typing import List

import docker

from dkrls import model

logger = logging.getLogger(__name__)


def get_images() -> List[model.Image]:
    """
    Get all images in the local Docker repository.

    If a single image is tagged with multiple tags, each tag will be returned
    as its own Image object.
    """
    client = docker.from_env()
    result = []

    for image in client.images.list():
        tags = image.tags
        if not tags:
            tags = ['<none>:<none>']

        for tag in tags:
            result.append(model.Image(
                tag,
                image.id.split(':')[1],
                image.history()[0]['Created'],
                sum(layer['Size'] for layer in image.history())
            ))

    return result


def get_repos() -> List[str]:
    """
    Get names of repositories in the local Docker registry.

    That is, collect just the repository part of a tag name and
    filter out duplicates.
    """
    tags = [
        image.repository for image in get_images()
    ]
    repos = sorted(set(tags))
    logger.debug(f'Converted {len(tags)} tags into {len(repos)} repos.')
    return repos
